<?php
return [
    // 聚付提供的系统码
    'sys_code' => '',
    // 聚付提供的商户号
    'account' => '',
    // 聚付提供的加密key
    'md5_key' => '',
    // 微信或支付宝应用APPID
    'auth_app_id' => '',

    'notify' => [
        // 支付宝H5支付回调
        'H5_ZFBWEB' => '',
        // 银联H5支付
        'API_QWAB' => '',
        // 微信H5支付
        'H5_WXWEB' => '',
        // 微信公众号支付
        'H5_WXJSAPI' => '',
        // 微信公众号支付
        'API_WXAPPLET' => '',
    ],

    'callback_url' => '',

    'refund_notify' => '',

    'request_url' => '',
	
    'signkey' => '',   #RSA签名秘钥，渠道分配
    
	'pubKey' => ''
];