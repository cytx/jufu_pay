<?php


namespace Ycbl\YinlianPay\cancel;


use Ycbl\YinlianPay\RequestBase;

class CancelOrder extends RequestBase
{
    public $trans_time;

    public $app_id;

    public $trans_id;

    public $memo;

    public function __construct()
    {
        parent::__construct();
    }

    public function getRequestSignBody()
    {
        return [
            'syscode' => $this->sys_code,
            'account' => $this->account,
            'trans_time' => date("YmdHis"),
            'app_id' => $this->app_id,
            'trans_id' => $this->trans_id,
        ];
    }

    public function getRequestBody($signData)
    {
        return $signData;
    }

    public function setConfig($config){
        $this->sys_code = $config['sys_code'];
        $this->account = $config['account'];
        $this->config = $config;
        $this->sign_key = $config['sign_key'];
        $this->request_url = $config['request_url'] . '/cgi-bin/n_pay_cancel.api';
    }
}