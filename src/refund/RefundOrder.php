<?php


namespace Ycbl\YinlianPay\refund;


use Ycbl\YinlianPay\RequestBase;

class RefundOrder extends RequestBase
{
    public $trans_time;

    public $amount;

    public $app_id;

    public $trans_id;

    public $notify_url;

    public $memo;

    public function __construct()
    {
        parent::__construct();
    }

    public function getRequestSignBody(): array
    {
        return [
            'syscode' => $this->sys_code,
            'account' => $this->account,
            'trans_time' => date("YmdHis"),
            'amount' => $this->amount,
            'app_id' => $this->app_id,
            'trans_id' => $this->trans_id,
            'notify_url' => $this->notify_url,
        ];

    }

    public function getRequestBody($signData): array
    {
        return $signData;
    }

    public function setConfig($config){
        $this->sys_code = $config['sys_code'];
        $this->account = $config['account'];
        $this->config = $config;
        $this->sign_key = $config['sign_key'];
        $this->request_url = $config['request_url'] . '/cgi-bin/n_single_refund.api';
    }
}