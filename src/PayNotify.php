<?php


namespace Ycbl\YinlianPay;


use Hyperf\Contract\ConfigInterface;

class PayNotify
{
    protected $data;

    protected $sys_code;

    protected $account;

    protected $error;

    protected $pub_key;

    public function __construct(array $data, $config)
    {
        $this->sys_code = $config['sys_code'];
        $this->account = $config['account'];
        $this->pub_key = $config['pubKey'];
        $this->data = $data;
    }

    public function verifySign(): bool
    {
        $params = $this->data;
        $verifyData = $params['sign'];
        unset($params['sign']);
        // 构造公钥
        $publicKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($this->getPubKey(), 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";
        // 构造公钥
        $key = openssl_get_publickey($publicKey);
        $params = urldecode(http_build_query(self::arr_sort($params)));
        // 验签 1：成功，0：失败
        $ok = openssl_verify($params, hex2bin($verifyData), $key, 'SHA256');
        // 释放密钥
        openssl_free_key($key);
        if ($ok == 1) {
            return true;
        } elseif ($ok == 0) {
            $this->error = "signature error";
            return false;
        } else {
            $this->error = "check sign";
            return false;
        }
    }
    public static function arr_sort($arr){
        ksort($arr);
        reset($arr);
        return $arr;
    }

    public function getError()
    {
        return $this->error;
    }

    /**
     * 获取公钥
     * @return mixed
     */
    public function getPubKey()
    {
        return $this->pub_key;
    }
}