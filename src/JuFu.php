<?php

namespace Ycbl\YinlianPay;

use GuzzleHttp\Exception\GuzzleException;
use Hyperf\Guzzle\ClientFactory;


class JuFu
{
    /**
     * @var ClientFactory
     */
    protected $client;

    /**
     * @var string
     */
    protected $result = "";

    /**
     * @var bool
     */
    protected $error = false;

    /**
     * @var string
     */
    protected $errorMessage = "";

    /**
     * @var array
     */
    protected $requestData = [];

    /**
     * @var array
     */
    protected $body = [];

    public function __construct()
    {
        $this->client = make(ClientFactory::class);
    }

    /**
     * 执行接口请求RSA方式
     * @param RequestBase $requestBase
     */
    public function execute(RequestBase $requestBase, $config)
    {
        // 设置配置参数
        $requestBase->setconfig($config);
        // 获取所有参数
        $data = $requestBase->getRequestSignBody();
        $data = $requestBase->getRequestBody($data);
        // 生成签名
        $sign = Sign::rsaSignData($data, $requestBase->getSignKey());
        $data['sign'] = $sign;
        $this->postGbk($requestBase->getUrl(), $data);
    }


    /**
     * 使用guzzle发送GBK编码 post请求
     * @param $url
     * @param $data
     */
    private function postGbk($url, $data)
    {
        $client = $this->client->create();
        try {
            $this->requestData = ['form_params' => $data, 'headers' => ['Content-type' => 'application/x-www-form-urlencoded', 'charset' => 'GBK']];
            $result = $client->request('post', $url, ['form_params' => $data, 'headers' => ['Content-type' => 'application/x-www-form-urlencoded', 'charset' => 'GBK']]);
            $return_data = $result->getBody()->getContents();
            $this->result = mb_convert_encoding($return_data, "UTF-8", "GBK");
            $this->parseResult();
        } catch (GuzzleException $e) {
            /**
             * @method string getMessage()
             * @method \Throwable|null getPrevious()
             * @method mixed getCode()
             * @method string getFile()
             * @method int getLine()
             * @method array getTrace()
             * @method string getTraceAsString()
             */
            $this->setError('requestData:' . json_encode($data, 256)
                . ',--GuzzleException->getMessage:' . $e->getMessage()
            );
            return;
        }

    }

    private function parseResult()
    {
        $data = json_decode($this->result, true);
        if (!$data) {
            $this->setError("失败");
            return;
        }
        if (isset($data['errorcode']) && $data['errorcode'] != "0000") {
            $this->setError($data['errormessage'] . ',--result:' . json_encode($data, 256) . ',--requestData:' . json_encode($this->requestData, 256));
            return;
        }

        unset($data['errorcode']);
        unset($data['errormessage']);
        $data['response']['sign'] = $data['sign'];
        $this->body = $data['response'];
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @return array []
     */
    public function getRequestData(): array
    {
        return $this->requestData;
    }

    /**
     * @param string $message
     */
    private function setError(string $message = "")
    {
        $this->errorMessage = $message;
        $this->error = true;
    }

    /**
     * @return bool
     */
    public function getError(): bool
    {
        return $this->error;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

}