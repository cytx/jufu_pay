<?php


namespace Ycbl\YinlianPay;


class Sign
{
    /**
     * 获取签名加密key
     * @param $data
     * @param string $key
     * @return string
     */
    public static function parseSignData($data, string $key = ""): string
    {
        if (is_array($data)) {
            $md5Str = urldecode(http_build_query(self::arr_sort($data)));
        } else {
            $md5Str = $data;
        }
        $md5Str = $md5Str . "&key=" . $key;
        return strtoupper(md5($md5Str));
    }

    /**
     * RSA加密签名
     * @param $data
     * @param string $private_key
     * @return string
     */
    public static function rsaSignData($data, $private_key): string
    {
        // 排序 拼接
        $data = urldecode(http_build_query(self::arr_sort($data)));
        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($private_key, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";
        $key = openssl_get_privatekey($privateKey);
        // 加密签名 $signature
        openssl_sign($data, $signature, $key, "SHA256");
        // 释放密钥
        openssl_free_key($key);
        // 16进制
        return bin2hex($signature);
    }

    /**
     * 数组排序
     * @param $arr
     * @return mixed
     */
    public static function arr_sort($arr)
    {
        ksort($arr);
        reset($arr);
        return $arr;
    }

}