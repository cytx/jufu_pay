<?php


namespace Ycbl\YinlianPay\pay;


use ReflectionClass;

class OrderOption
{
    // 交易时间YYYYMMDDHHMMSS
    protected $trans_time;
    // 订单金额
    protected $amount;
    // 支付方式
    protected $pay_mode;
    // 结算周期
    protected $aging;
    // 商户订单号
    protected $app_id;
    // 成功通知回调地址
    protected $callback_url;
    // 异步通知回调地址
    protected $notify_url;
    // 终端IP
    protected $terminal_ip;
    // 终端设备
    protected $terminal_dev;
    // 交易类型
    protected $trans_type;
    // 担保交易有效期
    protected $valid_date;
    // 借贷类型
    protected $dc_type;
    // 银行号
    protected $bank_id;
    // 银行卡号
    protected $card_no;
    // 银行卡名称
    protected $card_name;
    // 银行卡开户行
    protected $bank_name;
    // 用户标识
    protected $user_id;
    // 微信应用APPID
    protected $auth_app_id;
    // 微信用户标识
    protected $openid;
    // 是否原生支付
    protected $is_raw;
    // 卖家支付宝用户标识
    protected $seller_id;
    // 买家支付宝用户标识
    protected $buyer_id;
    // 支付授权码
    protected $auth_code;
    // 商品描述
    protected $subject;
    // 商品详情
    protected $body;
    // 优惠金额
    protected $discountable_amount;
    // 是否限制信用卡
    protected $limit_credit_pay;
    // 花呗分期数
    protected $hb_fq_num;
    // 订单支付有效期时
    protected $expire_date;
    // 该笔订单允许的最晚付款时间
    protected $time_expire;
    // 商品操作员编号
    protected $operator_id;
    // 商户门店编号
    protected $store_id;
    // 商户机具终端编号
    protected $terminal_id;
    // 分账标识
    protected $split_type;
    // 分账模式
    protected $split_mode;
    // 分账信息
    protected $split_list;
    // 备注说明
    protected $memo;
    // 交易签名
    protected $signature;

    /**
     * @param mixed $trans_time
     */
    public function setTransTime($trans_time): void
    {
        $this->trans_time = $trans_time;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @param mixed $pay_mode
     */
    public function setPayMode($pay_mode): void
    {
        $this->pay_mode = $pay_mode;
    }

    /**
     * @param mixed $aging
     */
    public function setAging($aging): void
    {
        $this->aging = $aging;
    }

    /**
     * @param mixed $app_id
     */
    public function setAppId($app_id): void
    {
        $this->app_id = $app_id;
    }

    /**
     * @param mixed $callback_url
     */
    public function setCallbackUrl($callback_url): void
    {
        $this->callback_url = $callback_url;
    }

    /**
     * @param mixed $notify_url
     */
    public function setNotifyUrl($notify_url): void
    {
        $this->notify_url = $notify_url;
    }

    /**
     * @param mixed $terminal_ip
     */
    public function setTerminalIp($terminal_ip): void
    {
        $this->terminal_ip = $terminal_ip;
    }

    /**
     * @param mixed $terminal_dev
     */
    public function setTerminalDev($terminal_dev): void
    {
        $this->terminal_dev = $terminal_dev;
    }

    /**
     * @param mixed $trans_type
     */
    public function setTransType($trans_type): void
    {
        $this->trans_type = $trans_type;
    }

    /**
     * @param mixed $valid_date
     */
    public function setValidDate($valid_date): void
    {
        $this->valid_date = $valid_date;
    }

    /**
     * @param mixed $dc_type
     */
    public function setDcType($dc_type): void
    {
        $this->dc_type = $dc_type;
    }

    /**
     * @param mixed $bank_id
     */
    public function setBankId($bank_id): void
    {
        $this->bank_id = $bank_id;
    }

    /**
     * @param mixed $card_no
     */
    public function setCardNo($card_no): void
    {
        $this->card_no = $card_no;
    }

    /**
     * @param mixed $card_name
     */
    public function setCardName($card_name): void
    {
        $this->card_name = $card_name;
    }

    /**
     * @param mixed $bank_name
     */
    public function setBankName($bank_name): void
    {
        $this->bank_name = $bank_name;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @param mixed $auth_app_id
     */
    public function setAuthAppId($auth_app_id): void
    {
        $this->auth_app_id = $auth_app_id;
    }

    /**
     * @param mixed $openid
     */
    public function setOpenid($openid): void
    {
        $this->openid = $openid;
    }

    /**
     * @param mixed $is_raw
     */
    public function setIsRaw($is_raw): void
    {
        $this->is_raw = $is_raw;
    }

    /**
     * @param mixed $seller_id
     */
    public function setSellerId($seller_id): void
    {
        $this->seller_id = $seller_id;
    }

    /**
     * @param mixed $buyer_id
     */
    public function setBuyerId($buyer_id): void
    {
        $this->buyer_id = $buyer_id;
    }

    /**
     * @param mixed $auth_code
     */
    public function setAuthCode($auth_code): void
    {
        $this->auth_code = $auth_code;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body): void
    {
        $this->body = $body;
    }

    /**
     * @param mixed $discountable_amount
     */
    public function setDiscountableAmount($discountable_amount): void
    {
        $this->discountable_amount = $discountable_amount;
    }

    /**
     * @param mixed $limit_credit_pay
     */
    public function setLimitCreditPay($limit_credit_pay): void
    {
        $this->limit_credit_pay = $limit_credit_pay;
    }

    /**
     * @param mixed $hb_fq_num
     */
    public function setHbFqNum($hb_fq_num): void
    {
        $this->hb_fq_num = $hb_fq_num;
    }

    /**
     * @param mixed $expire_date
     */
    public function setExpireDate($expire_date): void
    {
        $this->expire_date = $expire_date;
    }

    /**
     * @param mixed $time_expire
     */
    public function setTimeExpire($time_expire): void
    {
        $this->time_expire = $time_expire;
    }

    /**
     * @param mixed $operator_id
     */
    public function setOperatorId($operator_id): void
    {
        $this->operator_id = $operator_id;
    }

    /**
     * @param mixed $store_id
     */
    public function setStoreId($store_id): void
    {
        $this->store_id = $store_id;
    }

    /**
     * @param mixed $terminal_id
     */
    public function setTerminalId($terminal_id): void
    {
        $this->terminal_id = $terminal_id;
    }

    /**
     * @param mixed $split_type
     */
    public function setSplitType($split_type): void
    {
        $this->split_type = $split_type;
    }

    /**
     * @param mixed $split_mode
     */
    public function setSplitMode($split_mode): void
    {
        $this->split_mode = $split_mode;
    }

    /**
     * @param mixed $split_list
     */
    public function setSplitList($split_list): void
    {
        $this->split_list = $split_list;
    }

    /**
     * @param mixed $memo
     */
    public function setMemo($memo): void
    {
        $this->memo = $memo;
    }

    /**
     * @param mixed $signature
     */
    public function setSignature($signature): void
    {
        $this->signature = $signature;
    }

    public function getOptionData(OrderOption $option): array
    {
        $data = get_object_vars($option);
        return array_filter($data, function ($val) {
            return $val != null;
        });
    }
}