<?php


namespace Ycbl\YinlianPay\pay;


class H5WxWeb extends CreateOrder
{
    protected $pay_mode = "H5_WXWEB";

    public $amount;

    public $app_id;

    public $time_expire;

    public $is_raw;

    public $auth_app_id;

    public function getBody(): array
    {
        $data = [
            'aging' => $this->aging,
            'time_expire' => $this->time_expire,
            'auth_app_id' => $this->config['auth_app_id'],
            'is_raw' => $this->is_raw,
            'callback_url' => $this->config['callback_url'] ?? ''
        ];
        return array_filter($data, function ($val) {
            return $val != null;
        });
    }
}