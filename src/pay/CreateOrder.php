<?php

namespace Ycbl\YinlianPay\pay;

use Ycbl\YinlianPay\RequestBase;

class CreateOrder extends RequestBase
{
    // 交易时间YYYYMMDDHHMMSS
    protected $trans_time;
    // 订单金额
    protected $amount;
    // 支付方式
    protected $pay_mode;
    // 结算周期
    protected $aging;
    // 商户订单号
    protected $app_id;
    // 成功通知回调地址
    protected $callback_url;
    // 异步通知回调地址
    protected $notify_url;
    // 终端IP
    protected $terminal_ip;
    // 终端设备
    protected $terminal_dev;
    // 交易类型
    protected $trans_type;
    // 担保交易有效期
    protected $valid_date;
    // 借贷类型
    protected $dc_type;
    // 银行号
    protected $bank_id;
    // 银行卡号
    protected $card_no;
    // 银行卡名称
    protected $card_name;
    // 银行卡开户行
    protected $bank_name;
    // 用户标识
    protected $user_id;
    // 微信应用APPID
    protected $auth_app_id;
    // 微信用户标识
    protected $openid;
    // 是否原生支付
    protected $is_raw;
    // 卖家支付宝用户标识
    protected $seller_id;
    // 买家支付宝用户标识
    protected $buyer_id;
    // 支付授权码
    protected $auth_code;
    // 商品描述
    protected $subject;
    // 商品详情
    protected $body;
    // 优惠金额
    protected $discountable_amount;
    // 是否限制信用卡
    protected $limit_credit_pay;
    // 花呗分期数
    protected $hb_fq_num;
    // 订单支付有效期时
    protected $expire_date;
    // 该笔订单允许的最晚付款时间
    protected $time_expire;
    // 商品操作员编号
    protected $operator_id;
    // 商户门店编号
    protected $store_id;
    // 商户机具终端编号
    protected $terminal_id;
    // 分账标识
    protected $split_type;
    // 分账模式
    protected $split_mode;
    // 分账信息
    protected $split_list;
    // 备注说明
    protected $memo;
    // 交易签名
    protected $signature;

    /**
     * @var OrderOption
     */
    public $createOption;

    public function __construct()
    {
        parent::__construct();
        $this->aging = '1';
    }

    public function getRequestBody($signData): array
    {
        $optionData = [];
        if ($this->createOption != null) {
            $optionData = $this->createOption->getOptionData($this->createOption);
        }
        return array_merge($signData, $this->getBody(), $optionData);

    }

    public function getRequestSignBody(): array
    {
        return [
            'syscode' => $this->sys_code,
            'account' => $this->account,
            'trans_time' => date("YmdHis"),
            'pay_mode' => $this->pay_mode,
            'amount' => $this->amount,
            'app_id' => $this->app_id,
            'notify_url' => $this->notify_url,
        ];
    }

    public function getBody(): array
    {
        return [
            'aging' => $this->aging
        ];
    }

    public function setConfig($config){
        $this->sys_code = $config['sys_code'];
        $this->account = $config['account'];
        $this->notify_url = $config['notify'];
        $this->config = $config;
        $this->sign_key = $config['sign_key'];
        $this->request_url = $this->config['request_url'] . '/cgi-bin/n_web_pay.api';
    }
}