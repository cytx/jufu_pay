<?php


namespace Ycbl\YinlianPay\pay;


class ApiQWeb extends CreateOrder
{
    public $pay_mode = "API_CUAPP";

    public $amount;

    public $app_id;

    public $time_expire;

    public function getBody(): array
    {
        $data = [
            'aging' => $this->aging,
            'time_expire' => $this->time_expire,
            'callback_url' => $this->config['callback_url'] ?? ''
        ];
        return array_filter($data, function ($val) {
            return $val != null;
        });
    }
}