<?php


namespace Ycbl\YinlianPay;


use Hyperf\Contract\ConfigInterface;

abstract class RequestBase
{
    protected $request_url;

    protected $sys_code;

    protected $account;

    protected $md5_key;

    protected $notify;

    protected $config;

    protected $sign_key;

    public function __construct()
    {

    }

    /**
     * 获取待签名数据
     * @return mixed
     */
    public abstract function getRequestSignBody();

    /**
     * 获取请求体
     * @return mixed
     */
    public abstract function getRequestBody($signData);

    /**
     * 获取加密key
     * @return mixed
     */
    public function getMd5Key()
    {
        return $this->md5_key;
    }

    /**
     * 获取私钥
     * @return mixed
     */
    public function getSignKey()
    {
        return $this->sign_key;
    }

    /**
     * 获取新请求地址
     * @return mixed
     */
    public function getUrl()
    {
        return $this->request_url;
    }
}